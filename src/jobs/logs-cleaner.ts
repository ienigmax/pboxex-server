import job from 'node-cron';
import {logModel} from "../models";
import moment from "moment/moment";

export const invoke = async () => {
    try {
        console.log(`Log flush service started at ${moment().toDate()}`)
        let olderTs = moment().subtract(5, 'hours').toDate();
        await logModel.remove({createdAt: {$lte: olderTs}})
    } catch (e) {
        console.error(e);
    }
}


export const logsCleanerJob = () => {
    job.schedule(process.env.FREQ_CLEAN_LOGS, () => {
        invoke().then();
    })
}
