import passport from "passport";
import passportJwt from "passport-jwt";
import passportLocal from "passport-local";
import { userModel as User } from "../models";

const LocalStrategy = passportLocal.Strategy;
const JwtStrategy = passportJwt.Strategy;
const ExtractJwt  = passportJwt.ExtractJwt;
const { JWT_SALT } = process.env;

passport.use('jwt-strategy', new JwtStrategy(
    {
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: JWT_SALT
    }, function (jwtToken, done) {
        // console.log(jwtToken)
        User.findOne({ uuid: jwtToken.uid }, function (err, user) {
            if (err) { return done(err, false); }
            if (user) {
                return done(undefined, user , jwtToken);
            } else {
                return done(undefined, false);
            }
        });
}));

passport.use('local-strategy', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true,
}, async (req, username, password, done) =>{

    User.findOne({ username: username.toLowerCase() }, (err, user: any) => {
        if (err) { return done(err); }
        if (!user) {
            return done(undefined, false, { message: `username ${username} not found.` });
        }
        console.log('----')

        user.comparePassword(password, (err: Error, isMatch: boolean) => {
            if (err) { return done(err); }
            if (isMatch) {
                return done(undefined, user);
            }
            return done(undefined, false, { message: "Invalid username or password." });
        });
    });
}));
