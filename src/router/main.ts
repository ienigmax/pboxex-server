import { Router } from 'express';
import {authController, missionController, verify as verifyJWT} from '../controllers'
const router = Router();

router.use('/api/v1/auth', authController);
router.use('/api/v1/mission', verifyJWT, missionController);

export default router;
