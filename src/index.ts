// @ts-ignore
import './_env';
// @ts-ignore
import express from 'express';
// @ts-ignore
import router from './router/main'
import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser';
import db from './db';
import scripts from './scripts';
import cors from 'cors';
import corsConfig from './middlewares/cors'
import {logsCleanerJob} from "./jobs/logs-cleaner";

// Init scripts, like create a user
db().then(() => scripts().then());

const { PORT, HOST, NODE_ENV} = process.env;
const app = express();
app.use(cors(corsConfig));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(router);

// @ts-ignore
app.listen(PORT, HOST, () => {
    console.log(`${NODE_ENV} server is listening on ${process.env.PROTOCOL}://${HOST}:${PORT}`);
});
export default app
