import {common} from "./common";

describe("Common Functions", () => {
    it("uuid", () => {
        expect((common.genGUID()).length).toBe(36)
    });
})
