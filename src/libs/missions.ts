// @ts-ignore
import axios, { Axios, AxiosRequestConfig } from 'axios';
import {IMission} from "../models";
const { MS_HOST, MS_PORT } = process.env;
const CryptoTS = require("crypto-ts");


interface Mission {
    [key: string]: object
}

interface IMissionResponse {
    status: boolean;
    data: string | Array<any> | object
}

class Missions extends Axios {
    constructor(config: AxiosRequestConfig) {
        super(config);
    }

    /**
     * Gets list and count of the missions from the missions service
     * @param limit
     * @param page
     */
    public async getMissionList(limit: number, page: number): Promise<{ data?: { list: Array<IMission>, count: number }, msg?: string, status: number }> {
        try {
            // @ts-ignore
            let result = await this.get('/api/list', { params: { limit, page } });
            let parsed = JSON.parse(result.data);
            const bytes  = CryptoTS.AES.decrypt(parsed.msg.toString(), process.env.CRYPT_SALT);
            const decryptedData = JSON.parse(bytes.toString(CryptoTS.enc.Utf8));
            return { data: decryptedData, status: result.status };
        } catch (e) {
            console.error(e);
            return { msg: e.message, status: 400 };
        }
    }

    /**
     * Sends a create request to the missions service
     * @param mission
     */
    public async createMission(mission: IMission): Promise<{ data?: IMission, msg?: string, status: number }>  {
        try {
            const crypt = CryptoTS.AES.encrypt(JSON.stringify(mission), process.env.CRYPT_SALT).toString();
            // @ts-ignore
            // let result = await this.post('/api/', {msg: crypt}); // For some reason always sent GET requests
            let result = await axios.post(`http://${MS_HOST}:${MS_PORT}/api/`, {msg: crypt});
            let parsed = (result.data);

            const bytes  = CryptoTS.AES.decrypt(parsed.msg.toString(), process.env.CRYPT_SALT);
            const decryptedData = JSON.parse(bytes.toString(CryptoTS.enc.Utf8));
            return { data: decryptedData, status: result.status };
        } catch (e) {
            console.error(e);
            let msg = 'Something went wrong... Error: '+ e.message, status = 400;
            if(e.response?.status === 400) {
                const bytes  = CryptoTS.AES.decrypt(e.response?.data?.msg.toString(), process.env.CRYPT_SALT);
                msg = bytes.toString(CryptoTS.enc.Utf8);
            }
            return { msg, status };
        }
    }

    /**
     * Sends an update request to the missions service
     * @param mission
     * @param key
     */
    public async updateMessage(mission: IMission, key: string): Promise<{ data?: any, msg?: string, status: number }>  {
        try {
            const crypt = CryptoTS.AES.encrypt(JSON.stringify(mission), process.env.CRYPT_SALT).toString();
            // @ts-ignore
            // let result = await this.put('/api/' + key, {msg: crypt});
            let result = await axios.put(`http://${MS_HOST}:${MS_PORT}/api/` + key, {msg: crypt});
            let parsed = (result.data);

            const bytes = CryptoTS.AES.decrypt(parsed.msg.toString(), process.env.CRYPT_SALT);
            const decryptedData = JSON.parse(bytes.toString(CryptoTS.enc.Utf8));
            return { data: decryptedData, status: result.status };
        } catch (e) {
            console.error(e);
            let msg = 'Something went wrong... Error: '+ e.message, status = 400;
            if(e.response?.status === 400) {
                const bytes  = CryptoTS.AES.decrypt(e.response?.data?.msg.toString(), process.env.CRYPT_SALT);
                msg = bytes.toString(CryptoTS.enc.Utf8);
            }
            return { msg, status };
        }
    }

    /**
     * Sends a delete request to the missions service
     * @param key
     */
    public async deleteMessage(key: string): Promise<{ data?: any, msg?: string, status: number }>  {
        try {
            let crypt = CryptoTS.AES.encrypt(key, process.env.CRYPT_SALT).toString();
            // @ts-ignore
            // let result = await this.delete(`http://${MS_HOST}:${MS_PORT}/api/`, { params: { msg: crypt } });
            let result = await axios.delete(`http://${MS_HOST}:${MS_PORT}/api/` + base64Encode(crypt));
            console.log(result.data)
            const bytes  = CryptoTS.AES.decrypt(result.data.msg.toString(), process.env.CRYPT_SALT);
            const decryptedData = JSON.parse(bytes.toString(CryptoTS.enc.Utf8));
            return { data: decryptedData, status: result.status };
        } catch (e) {
            console.error(e);
            let msg = 'Something went wrong... Error: '+ e.message, status = 400;
            if(e.response?.status === 400) {
                const bytes  = CryptoTS.AES.decrypt(e.response?.data?.msg.toString(), process.env.CRYPT_SALT);
                msg = bytes.toString(CryptoTS.enc.Utf8);
            }
            return { msg, status };
        }
    }
}

/**
 * Helper function - encode to base64
 * @param str
 */
let base64Encode = (str) => Buffer.from(str).toString('base64');

/** Create and export an instance of the missions service */
const missionsService = new Missions({
    baseURL: `http://${MS_HOST}:${MS_PORT}`,
    timeout: 30000,
    headers: {'Content-type':'application/json'},
})

export { missionsService }
