import { v4 } from 'uuid';
export const common = {
    genGUID: () => v4(),
    sleep: (msec: number) => new Promise(((resolve, reject) => setTimeout(() => resolve, msec * 1000)))
}
