import clearAllMocks = jest.clearAllMocks;
import { userModel } from "../models";
import {common} from "../libs";
import moment = require("moment");
import {initScript} from "./init";

const { INIT_DB } = process.env;

describe('Startup script', () => {

    beforeEach(() => {
        clearAllMocks();
    });

    let uuid = common.genGUID();
    let user = {
        first_name: 'Test',
        last_name: 'Check',
        email: 'test.check63@mailinator.com',
        password: 'paybox',
        username: 'paybox',
        uuid: uuid
    }

    let response = {
        createdAt: moment().format('YYYY-MM-DDTHH:mm:ss'),
        id: "60156afff6da7f1180a6ba3d",
        key: "e",
        updatedAt: moment().format('YYYY-MM-DDTHH:mm:ss'),
        value: "{}",
        __v: 0,
        _id: "__MOCK__"
    }

    // @ts-ignore
    jest.spyOn(userModel, 'create').mockReturnValue(response)

    it('Should run only if INIT_DB is 1', () => {
        expect(INIT_DB).toBeDefined();
        expect(+INIT_DB).toBe(1)
    });

    // @todo - complete here
    // it('Should create a user paybox', () => {
        // expect(userModel.create).toBeCalledTimes(1);
        // expect(userModel.create).toHaveBeenCalledWith(user)
        // expect(initScript).toHaveBeenCalledTimes(1);
        // expect(initScript).toHaveBeenCalledTimes(1);
    // })
})
