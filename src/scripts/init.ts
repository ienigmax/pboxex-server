let { INIT_DB } = process.env;

import {common} from "../libs";
import { userModel } from '../models'

export const initScript = async () => {
    // @ts-ignore
    if(INIT_DB == 0) return console.log('Toggled off')
    console.log('======= CREATE A USER =======')
    try {
        await userModel.create({
            first_name: 'Test',
            last_name: 'Check',
            email: 'test.check63@mailinator.com',
            password: 'paybox',
            username: 'paybox',
            uuid: common.genGUID()
        });
    } catch (e) {
        console.error(e);
    }
    console.log('===== END CREATE A USER =====')
    return null;
}
