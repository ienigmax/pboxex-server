import { initScript } from './init';
import { runJobs } from './run-jobs';
export default async () => {
    initScript().then();
    runJobs();
};
