import './index';

describe('env', () => {

    test('NODE_ENV is test', () => {
        expect(['unit_test', 'test', 'development'].includes(process.env.NODE_ENV as string)).toBeTruthy();
    });

    test('env variables are loaded', () => {
        // Web
        expect(process.env.PROTOCOL).toBeDefined();
        expect(process.env.HOST).toBeDefined();
        expect(process.env.PORT).toBeDefined();
        expect(process.env.CORS_WL).toBeDefined();

        // DB
        expect(process.env.DB_HOST).toBeDefined();
        expect(process.env.DB_USER).toBeDefined();
        expect(process.env.DB_PASS).toBeDefined();
        expect(process.env.DB_PORT).toBeDefined();
        expect(process.env.DB_DEFALT).toBeDefined();

        // INIT DB
        expect(process.env.INIT_DB).toBeDefined();

        // Security
        expect(process.env.JWT_SALT).toBeDefined();
        expect(process.env.CRYPT_SALT).toBeDefined();

        // Message service
        expect(process.env.MS_HOST).toBeDefined();
        expect(process.env.MS_PORT).toBeDefined();
    });

});
