import { Document, Schema, Model, model, Error } from "mongoose";

export interface IMission {
    key: string;
    value: object;
}

export interface IMissionSchema extends Document {
   key: string;
   value: object;
}

const missionSchema = new Schema({
    key: { type: String },
    value: { type: Object }
},{
    toJSON: {
        virtuals: true,
        getters: true,
    },
    timestamps: true
});

export const missionModel: Model<IMissionSchema> = model<IMissionSchema>('Mission', missionSchema);
