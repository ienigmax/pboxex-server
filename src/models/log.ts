import { Document, Schema, Model, model, Error } from "mongoose";
export enum OpType {
    CREATE = 'CREATE',
    READ = 'READ',
    UPDATE = 'UPDATE',
    DELETE = 'DELETE',
    LIST = 'LIST',
}

interface ILog extends Document {
    type: OpType;     // CRUD operation type
    stack: string;    // In case there is an error
    status: boolean;  // 1 - Success / 0 - Failure
    created_at: string;
    updated_at: string;
}

const logSchema = new Schema({
    type: { type: String, enum: Object.values(OpType) },
    stck: { type: String },
    status: { type: Boolean, default: false },
}, {
    toJSON: {
        virtuals: true,
        getters: true,
    },
    timestamps: true
})

export const logModel: Model<ILog> = model<ILog>('Log', logSchema);
