import {common, missionsService} from "../libs";

const {MongoClient} = require('mongodb');
import {logModel, OpType} from "./index";
import moment = require("moment");



describe('Test Models - Log', () => {

    it('should insert a log into collection', async () => {
        const mockLog = {_id: 'some-log-id', type: OpType.LIST, status: 'MOCK', stack: ''}
        const mockLogResponse = {_id: 'some-log-id', type: OpType.LIST, status: 'MOCK', stack: '', createdAt: moment().format("YYYY-MM-DDTHH:mm:ssZ"), updatedAt: moment().format("YYYY-MM-DDTHH:mm:ssZ")}
        //@ts-ignore
        jest.spyOn(logModel, 'create').mockResolvedValue(mockLog)
        jest.spyOn(logModel, 'findOne').mockResolvedValue(mockLogResponse)
        await logModel.create(mockLog);

        const insertedLog = await logModel.findOne({_id: 'some-user-id'});
        expect(insertedLog).not.toEqual(mockLog);
        expect(insertedLog).toEqual(mockLogResponse);
        expect(logModel.create).toBeCalledTimes(1)
    });
});
