import {common, missionsService} from "../libs";

const {MongoClient} = require('mongodb');
import { userModel } from "./index";



describe('Test Models - User', () => {

    it('should insert a user into collection', async () => {

        const mockUser = {_id: 'some-user-id', first_name: 'John', last_name: 'Jackson', email: 'vasyapupkin@mail.ru', username: 'john', password: 'Mock', uuid: common.genGUID()};
        //@ts-ignore
        jest.spyOn(userModel, 'create').mockResolvedValue(mockUser)
        jest.spyOn(userModel, 'findOne').mockResolvedValue(mockUser)
        await userModel.create(mockUser);

        const insertedUser = await userModel.findOne({_id: 'some-user-id'});
        expect(insertedUser).toEqual(mockUser);
    });
});
