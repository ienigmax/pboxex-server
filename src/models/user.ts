import { Document, Schema, Model, model, Error } from "mongoose";
import bcrypt from "bcrypt-nodejs";
export interface IUser extends Document {
    username: string
    email: string;
    first_name: string;
    last_name: string;
    password: string;
    uuid: { type: String, index: true } // field level
}

const userSchema = new Schema({
    username: { type: String, required: true, unique: true },
    email: { type: String, required: true, unique: true },
    first_name: { type: String, required: true },
    last_name: { type: String, required: true },
    password: {
        type: String,
        // get: (): undefined => undefined,
    },
    uuid: { type: String, required: true }
},
{
        toJSON: {
            virtuals: true,
            getters: true,
        },
    }
)

userSchema.pre<IUser>("save", function save(next) {
    const user = this;

    bcrypt.genSalt(10, (err, salt) => {
        if (err) { return next(err); }
        bcrypt.hash(this.password, salt, undefined, (err: Error, hash) => {
            if (err) { return next(err); }
            user.password = hash;
            // @ts-ignore
            next();
        });
    });
});

userSchema.virtual('fullName').get(function () {
    return `${this.first_name} ${this.last_name}`;
});

userSchema.methods.comparePassword = function (pass: string, callback: any) {
    console.log(pass)
    // @ts-ignore
    bcrypt.compare(pass, this.password, (err: Error, isMatch: boolean) => {
        console.log(err, isMatch)
        callback(err, isMatch);
    });
};


export const userModel: Model<IUser> = model<IUser>('User', userSchema);


