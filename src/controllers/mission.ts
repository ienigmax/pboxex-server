import {Router, Request, Response} from "express";
import {missionModel, logModel, OpType} from "../models";
import {missionsService} from "../libs";

export const missionController = Router();

missionController.get('/list', getEntityList);
missionController.get('/:key', getEntity);
missionController.post('/', createEntity);
missionController.put('/:key', updateEntity);
missionController.delete('/:key', deleteEntity);

export async function getEntityList(req: Request, res: Response) {
    try {
        const { limit, page } = req.query;
        // @ts-ignore
        let result = await missionsService.getMissionList(limit, page);
        let { list, count } = result.data;
        if(result.status === 400) return res.status(400).json({msg: 'Something went wrong...'})
        await logModel.create({type: OpType.LIST, status: !!list, stack: ''});
        return res.json({list, count});
    } catch (e) {
        console.error(e);
        await logModel.create({type: OpType.LIST, status: false, stack: e.stack});
        return res.status(400).json({msg: 'Could not get missions list'});
    }
}

async function getEntity(req: Request, res: Response) {
    try {
        let { key } = req.params;
        let entity = await missionModel.findOne({ key });
        await logModel.create({type: OpType.READ, status: !!entity, stack: ''});
        if(!entity) return res.status(400).json({msg: 'could not get mission'});
        return res.status(200).json(entity);
    } catch (e) {
        console.error(e);
        await logModel.create({type: OpType.READ, status: false, stack: e.stack});
        return res.status(400).json({msg: 'Could not get mission'});
    }
}

async function createEntity(req: Request, res: Response) {
    try {
        const { key, value } = req.body;
        if(!key) return res.status(400).json({msg: 'key missing'});
        if(!value) return res.status(400).json({msg: 'value missing'});
        let result = await missionsService.createMission({key, value});

        if(result.status !== 200 && result.msg) {
            await logModel.create({type: OpType.DELETE, status: false, stack: result.msg});
            return res.status(400).json({ msg: result.msg });
        }
        await logModel.create({type: OpType.CREATE, status: true, stack: ''});
        return res.status(200).json(result);
    } catch (e) {
        console.error(e);
        await logModel.create({type: OpType.CREATE, status: false, stack: e.stack});
        return res.status(400).json({msg: 'Could not create mission'});
    }
}

async function updateEntity(req: Request, res: Response) {
    try {
        const key = req.params.key;
        const { key: newKey, value } = req.body;
        // let entity = await missionModel.find({ key });
        // if(!entity) res.status(400).json({ msg: 'Mission not exists' });
        // let result = await missionsService.createMission({key, value});
        let result = await missionsService.updateMessage({ key: newKey, value }, key);
        if(result.status !== 200 && result.msg) {
            await logModel.create({type: OpType.UPDATE, status: false, stack: result.msg});
            return res.status(400).json({ msg: result.msg });
        }
        await logModel.create({ type: OpType.UPDATE, status: true, stack: '' });
        return res.status(200).json(result);
    } catch (e) {
        console.error(e);
        await logModel.create({ type: OpType.UPDATE, status: false, stack: e.stack });
        return res.status(400).json({msg: 'Could not edit mission'});
    }
}

async function deleteEntity(req: Request, res: Response) {
    try {
        const { key } = req.params;
        let result = await missionsService.deleteMessage(key );
        if(result.status !== 200 && result.msg) {
            await logModel.create({type: OpType.DELETE, status: false, stack: result.msg});
            return res.status(400).json({ msg: result.msg });
        }
        await logModel.create({type: OpType.DELETE, status: true, stack: ''});
        return res.status(200).json(result);
    } catch (e) {
        console.error(e);
        await logModel.create({type: OpType.DELETE, status: false, stack: e.stack});
        return res.status(400).json({msg: 'Could not create mission'});
    }
}

export default { missionController };
