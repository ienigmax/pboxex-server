import {missionsService} from "../libs";
import {logModel} from "../models";

import {getEntityList, missionController} from "./mission";
import moment = require("moment");
import {NextFunction} from "express";
import {getMockReq, getMockRes} from "@jest-mock/express";

describe('Test mission controllers', () => {
    jest.clearAllMocks();

    const mockNext: NextFunction = jest.fn();
    const mockResponse: any = {
        status: jest.fn(),
        json: jest.fn(),
    };

    let listRsponse = {
        data: {
            list: [],
            count: 0
        },
        status: 200
    }

    const mockRequest: any = {
        query: {
            limit: 1,
            page: 1
        }
    };



    let response = {
        createdAt: moment().format('YYYY-MM-DDTHH:mm:ss'),
        id: "60156afff6da7f1180a6ba3d",
        updatedAt: moment().format('YYYY-MM-DDTHH:mm:ss'),
        __v: 0,
        _id: "__MOCK__"
    }

    beforeEach(() => {
        jest.spyOn(missionsService, 'getMissionList').mockResolvedValue(listRsponse)
        // @ts-ignore
        jest.spyOn(logModel, 'create').mockResolvedValue(response)
    });

    const json = () => jest.fn()


    it('Should get list of missions', async () => {
        await getEntityList(mockRequest, mockResponse);
        expect(missionsService.getMissionList).toHaveBeenCalledTimes(1)
        expect(logModel.create).toHaveBeenCalledTimes(1)
    })
});

// describe('Controller Mocks', () => {
//     const req = getMockReq({ query: { limit: 1, count: 1 } })
//
//
//     let listRsponse = {
//         data: {
//             list: [],
//             count: 0
//         },
//         status: 200
//     }
//
//     const { res, next, clearMockRes } = getMockRes({listRsponse});
//
//     let response = {
//         createdAt: moment().format('YYYY-MM-DDTHH:mm:ss'),
//         id: "60156afff6da7f1180a6ba3d",
//         updatedAt: moment().format('YYYY-MM-DDTHH:mm:ss'),
//         __v: 0,
//         _id: "__MOCK__"
//     }
//
//     beforeEach(() => {
//         clearMockRes() // can also use clearMockRes()
//         // @ts-ignore
//         jest.spyOn(missionsService, 'getMissionList').mockResolvedValue(listRsponse)
//         // @ts-ignore
//         jest.spyOn(logModel, 'create').mockResolvedValue(response)
//     })
//
//     it('Should get list of missions', async () => {
//         await getEntityList(req, res)
//
//         expect(res.json).toBeDefined()
//     });
// })
