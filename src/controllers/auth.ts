import {NextFunction, Request, Response, Router} from 'express';
import passport from 'passport';
import('../utils/auth');
import * as jwt from "jsonwebtoken";
const { JWT_SALT } = process.env;
export const authController = Router();
authController.get('/', ((req, res) =>  {
    res.json({msg: 'ol'})
}))
// @ts-ignore
authController.post('/login', login);
// @ts-ignore
authController.post('/verify', verify);

export function login(req: Request, res: Response, next: NextFunction) {
    // res.json({msg: 'ol'})
    passport.authenticate("local-strategy", function (err, user, info) {
        // no async/await because passport works only with callback ..

        if (err) return next(err);
        if (!user) {
            return res.status(401).json({ status: false, code: "unauthorized" });
        } else {
            // console.log(user.uuid)
            const token = jwt.sign({ uid: user.uuid }, JWT_SALT);
            let u = user.toJSON();
            delete u.password;
            res.status(200).send({ token: token, user: u });
        }
    })(req, res, next);
}


export function verify(req: Request, res: Response, next: NextFunction) {
    passport.authenticate('jwt-strategy', { session: false }, async (err, user, msg) => {
        // console.log(user);
        if(err) {
            console.log(err);
            return res.status(401).json({ success: false, code: "unauthorized" });
        } if(!user) {
            return res.status(402).json({ success: false, code: "unauthorized" });
        } else {
            return next()        }
    })(req, res, next);
}

export default { authController };
