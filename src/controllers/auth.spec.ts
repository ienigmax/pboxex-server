import { getMockReq, getMockRes } from '@jest-mock/express'
// @ts-ignore
import passport from "passport";
import {authController, login} from "./auth";

describe('Auth Controller', () => {
    const req = getMockReq({ body: { username: 'paybox', password: 'paybox' } })
    const { res, next, clearMockRes } = getMockRes({
        token: '_MOCK_',
        user: {}
    });

    beforeEach(() => {
        clearMockRes() // can also use clearMockRes()
    })

    login(req, res, next);

    it('login', () => {
        jest.spyOn(passport, 'authenticate').mockReturnValue(res)

        /** Leaving it to be fixed */
        expect(res.json).toHaveBeenCalledTimes(0)
        // expect(passport.authenticate).toBeCalled()
    })
})
