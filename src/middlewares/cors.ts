const whiteList = JSON.parse(process.env.CORS_WL);
export default {
    whiteList,
    allowedHeaders: [
        'Origin',
        'X-Requested-With',
        'Content-Type',
        'Authorization',
        'Accept',
        'X-Access-Token',
    ],
    credentials: true,
    methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
    origin: (origin, callback) => {
        if(!origin) return callback(null, true);
        if(!whiteList.includes(origin)){
            const msg = 'The CORS policy for this site does not allow access from the specified Origin.';
            return callback(new Error(msg), false);
        }
        return callback(null, true);
    },

}
