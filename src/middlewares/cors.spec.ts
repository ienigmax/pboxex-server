import cors from "./cors";

describe('cors-options', () => {
    it('cors', async () => {
        let origin1 = 'https://localhost:4200'
        let origin2 = 'https://localhost:4500'
        let err = new Error('The CORS policy for this site does not allow access from the specified Origin.')
        let cb1, cb2;
        let p = new Promise(((resolve, reject) => {
            cors.origin(origin1, resolve);
        }));
        let p2 = new Promise(((resolve, reject) => {
            cors.origin(origin2, resolve);
        }));

        expect(await p).toEqual(err)
        expect(await p2).toBeNull()
        // expect(cors.origin(origin2, null)).not.toBeTruthy()
    })
});
