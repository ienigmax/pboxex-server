## P-Box App

#### Description 
Goal of this project is to demonstrate an API built on Express and TypeScript.

This application serves as the main Web API of the P-Box application. This project consists of several parts:
1. Authentication service - Supports JWT authentication using `passport.js` library
2. Missions service library - sends messages to `pbox-missions` project (Missions service) - using `crypto-ts` providing 
AES encryption between the 2 services. 
3. Jobs Manager (Cron Jobs) - services that are triggered on a specific frequency.
You musty specify the frequency of the log remover in the relevant `.env` file in `FREQ_CLEAN_LOGS`  
#### Requirements
- `node.js` > 8 
- `MongoDB` must be installed and configured. It is recommended to install Robo 3t or any other IDE 
to work with the database

#### Run Project
The project supports multiple environments. 

#### *  **Note:** `INIT_DB` must be set to 1 on the first run, then set to 0, otherwise, a new user will be created on each new run

`npm run dev` - `development` environment (make sure to copy the properties from `.env.example` file)

`npm run prod` - run `production` environment

`pm2 start npm --name=papp-api -- run prod` - Using pm2 to run the service as a background process

#### References
* [Missions Service - microservice for saving missions](https://bitbucket.org/ienigmax/pboxex-missions/src/master/)
* [Client UI (Angular)](https://bitbucket.org/ienigmax/pboxex-client/src/master/)

